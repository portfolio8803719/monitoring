import java.io.IOException;
import java.io.InputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Scanner;

public class Monitoring {

// fonction exécution générale
// fonction qui peut lancer un script bash unique
// fonction qui lis la collection de script bash et la renvoie a la fonction d'exec générale
// fonciton qui fait l'output du buffer

public static void main(String[] args) {


	String val1 = args[0];
	//execBash(val1);
	//bufferRead(val1);
	switch (val1) {
		case "ping": execBash("ping.sh");
		break;
		case "ram": execBash("ram_usage.sh");
		break;
		case "cpu": execBash("cpu_usage.sh");
		break;
		case "g":
			  execBash("network_usage.sh");
                          execBash("cpu_usage.sh");
			  execBash("ping.sh");
                          execBash("ram_usage.sh");
		break;
		case "debit": execBash("network_usage.sh");
		break;
	}

}


public static void bufferReadOutput(Process proc){
        //String[] cmd = { "/bin/bash", "ping.sh" };
        //Process proc = Runtime.getRuntime().exec(cmd);
try {
        InputStream in = proc.getInputStream();
        BufferedReader buffer = new BufferedReader(new InputStreamReader(in));

        String line;

        while ((line = buffer.readLine()) != null) {
                //String[] cmdsplit = line.split("");
                System.out.println(line);
                //System.out.println("==================================");
        }

        in.close();
        buffer.close();

} catch (IOException e) {
  e.printStackTrace();
}

}

public static void execBash(String bashName){



try {
	String[] cmd = { "/bin/bash", bashName };
	Process proc = Runtime.getRuntime().exec(cmd);
	bufferReadOutput(proc);

} catch (IOException e) {
   e.printStackTrace();
}




}

}

