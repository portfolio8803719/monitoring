#!/bin/bash

# Doc
# cat /proc/cpuinfo | grep -i "cpu mhz"

# Mode de fonctionnement : raffraichissement du display ou non
# Option : refresh
# Utilistation : cpu_usage.sh <option>

mode=$1

# Fonction de calcul de l'utilisation deu cpu
function bash_exec() {
cpu_now=($(head -n1  /proc/stat))
cpu_sum="${cpu_now[@]:1}"
cpu_sum=$((${cpu_sum// /+}))
cpu_delta=$((cpu_sum - cpu_last_sum))
cpu_idle=$((cpu_now[4] - cpu_last[4]))
cpu_used=$((cpu_delta - cpu_idle))
cpu_usage=$((100 * cpu_used / cpu_delta))
cpu_last=("${cpu_now[@]}")
cpu_last_sum=$cpu_sum

# caractere permettant le clear de la derniere ligne
# en complémenarité avec tput
refresh_char="%\\033[5A"
# argument pour effectuer une nouvelle ligne avec `echo`
newline_arg="-e"

if [[ $mode != "refresh" ]]; then
refresh_char=""
newline_arg=""
fi

echo $newline_arg  "CPU usage at $cpu_usage$refresh_char" 

}


# Fonction de Rafraichissement du display
function refresh_display() {
n=0
tput sc
while (($n<5)); do
        get_cpu_usage
	((n++))
	sleep 0,5
tput rc;tput el
done

}

# Execution du script selon mode de display
if [[ $mode == "refresh" ]]; then
refresh_display
else
bash_exec
fi
