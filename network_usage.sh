#!/bin/bash

interface="enp0s3"
mode=""


while getopts "i:rh" options;
do
    case "$options" in
	i) interface=$OPTARG;;
        r) mode="refresh";;
	h)
	  echo "-i, --interface			Nom de l'interface réseau à surveiller"
	  echo "-m, --mode			Affichage brut ou graphique : refresh"
          echo "-h, --help			Affiche cette aide"
	  exit 0;;
    esac
done


function bash_exec() {
mbits_r=$(netstat -e -n -i | grep "$interface"  -A 5 | grep 'RX packets' | tail -l | awk '{print $5}')
mbits_s=$(netstat -e -n -i | grep "$interface"  -A 7 | grep 'TX packets' | tail -l | awk '{print $5}')

mbits_received=$(echo "($mbits_r*8)/1000000" | bc)
mbits_sent=$(echo "($mbits_s*8)/1000000" | bc)

# caractere permettant le clear de la derniere ligne
# en complémenarité avec tput
refresh_char="%\\033[5A"
# argument pour effectuer une nouvelle ligne avec `echo`
newline_arg="-e"

if [[ $mode != "refresh" ]]; then
 refresh_char=""
 newline_arg=""
fi

echo $newline_arg  "Débit reçu : $mbits_received mbits/s Débit envoyé : $mbits_sent mbits/s$refresh_char"
}

# Fonction de Rafraichissement du display
function refresh_display() {
 n=0
 tput sc
while (($n<5)); do
         bash_exec
         ((n++))
         sleep 0,5
tput rc;tput el
done
}

# Execution du script selon mode de display
if [[ $mode == "refresh" ]]; then
refresh_display
else
bash_exec
fi
