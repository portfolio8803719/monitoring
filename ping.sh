#!/bin/bash

adress=$1

packet_loss=$(ping -c 1 $adress | grep "packet loss" | awk '{print $6}')

#ping -c 4 ${adress} | grep "packet loss" | awk '{print $6}' && echo "${adress} OK" || echo "${adress} KO"
#result= $("${adress} OK" || "${adress} KO")

if [ "$packet_loss" ]
then

	if [ "$packet_loss" = "0%" ]; then
		echo "$adress OK"
	elif [  "$packet_loss" = "100%" ]; then
		echo "$adress KO"
		$(/bin/bash notification.sh $adress "down" > /dev/null 2>&1)
	else
                echo "$adress instable : totalité des packets non transmis"
	fi
else
	echo "Adresse incorrecte"
fi
