#!/bin/bash


#token=$(curl -k https://serveur.caue971.org:83/serverinfo-token.txt > token.txt)
token=$(cat token.txt)

#serverinfo=$(curl -k https://serveur.caue971.org:83/ocs/v2.php/apps/serverinfo/api/v1/info?format=json -H "NC-Token: $token")
serverinfo=$(cat serverinfo.txt)

mem_free=$(jq '.ocs.data.nextcloud.system.mem_free' <<< $serverinfo)
mem_total=$(jq '.ocs.data.nextcloud.system.mem_total' <<< $serverinfo)
mem_f_g=$(("$mem_free/1048576"))
#echo "Mémoire libre "$mem_f_g "Giga"
mem_t_g=$(("$mem_total/1048576"))
#echo "Mémoire total "$mem_t_g "Giga"

tot=$mem_t_g
free=$mem_f_g
mem_use_giga=$(($tot-$free))

calc(){ awk "BEGIN { print $* }"; }

mem_use_percent=$(calc $mem_use_giga/$tot*100 | xargs printf "%.2f" )
echo "Mémoire : $mem_f_g Go/$mem_t_g Go  Utilisée $mem_use_giga Go ($mem_use_percent%)"

#echo '{"mem_total": 0} | jq
#{
#	"mem_total": 0
#}
