# monitoring
### description

Afficher les informations du serveur en temps réel

### utilisation du script

`java Monitoring <option>`

Options disponibles :
-ram : afficher les informations de la ram
-cpu : afficher les informations du cpu
-ping : effectuer un ping
-debit : débit descendant et montant
-g : affiche l'ensemble des options ram, ping, cpu, debit
 



