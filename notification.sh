#!/bin/bash

title=$1
body=$2

curl -X "POST" "https://push.techulus.com/api/v1/notify/baf422c6-d64d-4bb1-95ac-f17c49900e33" \
-H 'Content-Type: application/json; charset=utf-8' \
-d $'{ "title" : "'"$title"'", "body" : "'"$body"'"}'
