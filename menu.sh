#!/bin/bash

       function all_script() {
        ram=$(/bin/bash ram_usage.sh)
        cpu=$(/bin/bash cpu_usage.sh)
        network=$(/bin/bash network_usage.sh)
        #ping=$(/bin/bash ping.sh)

        whiptail --title "ALL MIGHTY MONITORING" \
        --yesno "$ram\n$cpu\n$network" \
        --yes-button "actualiser" \
        --no-button "retour" 20 60
        }


function advanceMenu() {
	choiceresult=$(whiptail --title "Menu" --menu "Select options" 25 78 16 \
	"1" "ping" \
	"2" "Ram" \
	"3" "Cpu" \
	"4" "Débit" \
	"5" "Lancer tout" 3>&2 2>&1 1>&3)


exitstatus=$?

if [ $exitstatus = 0 ]; then

case $choiceresult in

	"1")
	echo "ping"
	#whiptail --title "Options 1" --msgbox  "you wiinn 1" 8 45
	#$(/bin/bash ping.sh)
	whiptail --title "Ping" --msgbox "$(/bin/bash ping.sh)" 20 60
	;;

	2)
	whiptail --title "ram usage" --msgbox "$(/bin/bash ram_usage.sh)" 20 60
	;;

	3)
	whiptail --title "Cpu" --msgbox "$(/bin/bash cpu_usage.sh)" 20 60
        ;;

	4)
        echo "Débit"
        whiptail --title "Débit" --msgbox "$(/bin/bash network_usage.sh -i enp0s3)" 20 60
        ;;


        5)
	n=0

        while (($n==0)); do
	all_script

        if [[ $? -eq 0 ]]; then
	 all_script
        elif [[ $? -eq 1 ]]; then
	 ((n++))
	 advanceMenu
	fi
        done
        ;;

	*)
	echo ":bye mf"
	;;
esac

else
echo "exited status"
fi

}

advanceMenu
